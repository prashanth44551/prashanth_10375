package com.abc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.abc.bean.Employee;
import com.abc.service.EmployeeService;
@Controller
public class EmployeeController {
    @Autowired
	private EmployeeService emp;
	
	 public EmployeeService getEmp() {
		return emp;
	}

	public void setEmp(EmployeeService emp) {
		this.emp = emp;
	}

	@GetMapping("/employeedetails")
	 public ModelAndView getEmployeeDetails() {
			Employee emp1=emp.createEmp();
			ModelAndView mav= new ModelAndView("employeeDetails","emp",emp1);
		    return  mav;
		}

}
