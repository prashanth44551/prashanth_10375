package com.student_registration.utilities;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

public class DataBaseConnector {
	private static  SessionFactory sessionFactory = buildSessionFactory();
	
		public static Connection getConnection() {
			Connection conn = null;
			try {
				Class.forName("com.mysql.jdbc.Driver");
			    conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/studentregistrationapp", "root", "innominds");
			} 
			catch (SQLException e) {
					e.printStackTrace();
			}
		     catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			return conn;
		
		}
	
		  private static SessionFactory buildSessionFactory() {
		        try { 
		            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder() .configure().build(); //configures setting from hibernate.cfg.xml
		            // Create MetadataSources
		            MetadataSources metadataSources = new MetadataSources(serviceRegistry);
		            // Create Metadata
		            Metadata metadata = metadataSources.getMetadataBuilder().build();
		            // Create SessionFactory
		            sessionFactory = metadata.getSessionFactoryBuilder().build();
		            return sessionFactory;
		        } catch (Throwable ex) {       
		            System.err.println("Initial SessionFactory creation failed." + ex);
		            throw new ExceptionInInitializerError(ex);
		        }
		    }
		  
		  public static SessionFactory getSessionFactory() {
		        return sessionFactory;
		    }
		  public static void shutdown() {
		        // Close caches and connection pools
		        getSessionFactory().close();
		    }
		
}
