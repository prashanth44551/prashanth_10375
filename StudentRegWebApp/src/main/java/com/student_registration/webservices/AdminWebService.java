package com.student_registration.webservices;

import java.sql.Date;
import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.student_registration.models.AdminHomeModel;
import com.student_registration.models.AdminModel;
import com.student_registration.models.PostModel;
import com.student_registration.models.RequestModel;
import com.student_registration.models.LoginInputModel;
import com.student_registration.services.AdminService;
import com.student_registration.utilities.JsonDeserializer;

@Path("/admin")
public class AdminWebService {
	/**
	 * this method takes the request and checks the student details in database
	 * @param userName
	 * @param password
	 * @return admin json object or null 
	 */
	@POST
	@Path("/login")
	@Consumes(MediaType.APPLICATION_JSON)
   
	public String login(String loginJson) {
	    LoginInputModel loginData = JsonDeserializer.getObject(loginJson,LoginInputModel.class);
		AdminService adminService = new AdminService();
		AdminHomeModel adminHomeModel = adminService.adminLogin(loginData.getUserName(),loginData.getPassword());
		Gson gson = new Gson();
		if(adminHomeModel != null) {
			return gson.toJson(adminHomeModel);
		}
		else {
			return gson.toJson(null);
		}
	}
	
	/**
	 * this method compare the student name and fav place return admin unique key in database 
	 * @param userName
	 * @param securityQue
	 * @return String unique key 
	 */
	@GET
	@Path("/forgotPassword/{userName}/{securityQue}")
	
	public String forgotPassword(@PathParam("userName") String userName,@PathParam("securityQue") String securityQue) {
		AdminService adminService = new AdminService();
		String adminKey = adminService.adminForgotPassWord(userName, securityQue);
		return adminKey;
	}
	

	/**
	 * this method calls the sigUp method of AdminService class ...register the admin into db
	 * 
	 * @param admin is AdminModel type
	 * @return true if admin registers successfully else false
	 */
	@PUT
	@Path("/signUp")
	@Produces("Application/json")


	public String signUpAdmin(String adminJson)  {
		AdminModel admin = JsonDeserializer.getObject(adminJson, AdminModel.class);
		AdminService adminService = new AdminService();
		System.out.println(admin.getAdminPassword());
		try {
		   adminService.signUpAdmin(admin);	
		   return new Gson().toJson(admin);
		   
		}catch (Exception e) {
			return new Gson().toJson(null);
		}
	}
	


	/**
	 * this method resets the admin password
	 * 
	 * @param object index or key of map ,new password details
	 * 
	 */
	
	@PUT
	@Path("/resetPassword/{newPassword}/{key}")
	
	@Produces("Application/json")
	public  String resetAdminPassword(@PathParam("newPassword")String newPassWord,@PathParam("key") String searchKey) {
		AdminService adminService = new AdminService();
		Boolean result = adminService.resetAdminPassword(newPassWord, Integer.parseInt(searchKey));		
	    return new Gson().toJson(result);
	
	}
	
	
	/**
	 * this method accesps the admin post and stores into the Posts database
	 * @param json of post info
	 * @return boolean
	 */
	@PUT
	@Path("/postFeed")
	public String postFeed(String adminPostJson) {
		PostModel adminPost = JsonDeserializer.getObject(adminPostJson,PostModel.class);
		 long millis=System.currentTimeMillis();  
		 adminPost.setDate(new Date(millis));
		 AdminService adminService = new AdminService();
		 Boolean status =  adminService.postFeed(adminPost);
		 return status.toString();
	}
	
	/**
	 * this method gets the student requests/complaints 
	 */
	@GET
	@Path("/request")
	public String getRequests() {
		 AdminService adminService = new AdminService();
		ArrayList<RequestModel>  requests = adminService.getRequests();
		return new Gson().toJson(requests);
	}
	
}
