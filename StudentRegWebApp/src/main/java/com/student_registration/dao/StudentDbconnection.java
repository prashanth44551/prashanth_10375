package com.student_registration.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;

import com.student_registration.models.AdminModel;
import com.student_registration.models.PostModel;
import com.student_registration.models.RequestModel;
import com.student_registration.models.StudentFeesModel;
import com.student_registration.models.StudentHomeModel;
import com.student_registration.models.StudentLoginModel;
import com.student_registration.models.StudentMarks;
import com.student_registration.models.StudentModel;
import com.student_registration.utilities.DataBaseConnector;
import com.student_registration.utilities.UtilityDao;
import com.sun.corba.se.impl.protocol.giopmsgheaders.RequestMessage;

/**
 * this class is used for All the CURD operations on student file
 * 
 * @author Group B
 *
 */
public class StudentDbconnection {

	/**
	 * 
	 * This method deletes the student information in all tables..
	 * 
	 * @param studentId
	 * @return bollean
	 */
	SessionFactory sf = null;
	Session session = null;
	Transaction transaction = null;

	public StudentDbconnection() {
		sf = DataBaseConnector.getSessionFactory();
		session = sf.openSession();
		transaction = session.beginTransaction();
	}

	public boolean deleteStudentById(String studentId) {
		boolean status = false;
		Connection con = DataBaseConnector.getConnection();

		try {

			PreparedStatement ps = con.prepareStatement("delete from student_table where studentId =?");
			PreparedStatement ps1 = con.prepareStatement("delete from student_authentication_table where studentId =?");
			PreparedStatement ps2 = con.prepareStatement("delete from student_marks where student_id = ?");
			ps.setString(1, studentId);
			int deletedRows = ps.executeUpdate();
			ps1.setString(1, studentId);
			int deletedRows1 = ps1.executeUpdate();
			ps2.setString(1, studentId);
			ps2.executeUpdate();

			if (deletedRows > 0 && deletedRows1 > 0) {
				status = true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		finally {
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return status;

	}

	/**
	 * this method regers the new student into the StudentInfo file
	 * 
	 * @param StudentModel Object
	 * @return true if the student is regestered else return false
	 */

	@SuppressWarnings("finally")
	public boolean registerStudent(StudentModel student) {
		boolean status = false;
		StudentLoginModel studentAuthInfo = new StudentLoginModel();
		studentAuthInfo.setUserName(student.getStudentId());
		studentAuthInfo.setPassWord("Inno@123");
		studentAuthInfo.setSecurityQue("myVillage");

		String authResult = (String) session.save(studentAuthInfo);
		String dataResult = (String) session.save(student);
		int feesResult = updateFees(student.getStudentId(), student.getFees());
		transaction.commit();
		System.out.println(authResult);
		if (authResult != null && dataResult != null) {
			status = true;
		}
		return status;
	}

	/**
	 * this method check user details in the data base
	 * 
	 * @param userName
	 * @param password
	 * @return
	 */

	public StudentHomeModel studentLogin(String userName, String password) {

		StudentHomeModel studentHomeModel = null;
		StudentModel student = null;

		Query query = session.createQuery("from StudentLoginModel where userName = :userName and password = :password");
		query.setParameter("userName", userName);
		query.setParameter("password", password);
		StudentLoginModel studentLoginInput = (StudentLoginModel) query.uniqueResult();

		if (studentLoginInput != null) {
			String studentId = studentLoginInput.getUserName();
			student = new UtilityDao().fetchStudentDetails(studentId);

			// getting the neews feed from the posts table
			ArrayList<PostModel> posts = new ArrayList<PostModel>();
			posts.addAll(session.createQuery("from PostModel order by date desc").list());

			// creating the student home page result object
			studentHomeModel = new StudentHomeModel();
			studentHomeModel.setNewsFeed(posts);
			studentHomeModel.setStudent(student);

		}
		return studentHomeModel;
	}

	/**
	 * This method takes the studentId and securityQuestion and validtaes weather
	 * user is valid or not. If valid gives chance to reset password for user
	 * 
	 * @param studentId
	 * @param securityQue
	 * @return
	 */

	public String studentForgotPassWord(String studentId, String securityQue) {
       
		Query query = session
				.createQuery("from StudentLoginModel where userName = :studentId and securityQue = :securityQue");
		
		Criteria cr = session.createCriteria(StudentLoginModel.class);
		Criterion nameCriteria = Restrictions.eq("userName",studentId);
		Criterion secCriteria = Restrictions.eq("securityQue",securityQue);
		LogicalExpression exp = Restrictions.and(nameCriteria,secCriteria);
		cr.add(exp);
		StudentLoginModel studentLoginInput = (StudentLoginModel)cr.uniqueResult();

		if (studentLoginInput != null) {
			return studentId;
		}

		return null;
	}

	public boolean resetStudentPassword(String newPassWord, String studentId, String favouritePlace) {
		boolean status = false;
		Query query = session.createQuery(
				"update  StudentLoginModel set passWord = :newPassWord,securityQue = :favouritePlace where studentId= :studentId ");
		query.setParameter("newPassWord", newPassWord);
		query.setParameter("favouritePlace", favouritePlace);
		query.setParameter("studentId", studentId);
		int res = query.executeUpdate();
	    transaction.commit();
		if (res > 0)
			status = true;

		return status;

	}

	/**
	 * this method updates the student data in the database param
	 * 
	 * @param studentId,fieldname,value
	 * @return boolean
	 */
	public boolean updateStudentField(String studentId, String fieldName, String value) {

		Boolean status = false;
		
			Query query = session.createQuery("update  StudentModel set " + fieldName + "  = :fieldName "
					+ " where studentId= :studentId ");

		    query.setParameter("fieldName", value);
			query.setParameter("studentId", studentId);
			int updatedRows = query.executeUpdate();
			transaction.commit();
			System.out.println(value);
			if (updatedRows > 0) {
				status = true;
			}

		return status;

	}

	/**
	 * this method checks the last student id and returns incremented value if that
	 * 
	 * @return studentName
	 */

	public String getNewStudentId() {

		Connection con = DataBaseConnector.getConnection();
		String newStudentId = "CN0010001";
		try {
			if (con == null) {
				throw new Exception("sorry some thing went wrong");
			}

			Statement statment = con.createStatement();
			ResultSet rs = statment.executeQuery("select studentId from student_table order by studentId desc limit 1");

			if (rs.next()) {
				String lastStudentId = rs.getString(1);
				int postFix = Integer.parseInt(lastStudentId.substring(4)) + 1;
				newStudentId = "CN00" + postFix;
			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return newStudentId;

	}

	/**
	 * this method inserts the fees details into student_fees_details table
	 * 
	 * @studentId,fees
	 * @int inserted rows count
	 */

	public int updateFees(String studentId, double fees) {
		int transactionId = 0;
		if (fees > 0) {
			Date dt = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String currentTime = sdf.format(dt);
			StudentFeesModel studentFeesModel = new StudentFeesModel();
			studentFeesModel.setPaidDate(currentTime);
			studentFeesModel.setStudentId(studentId);
			studentFeesModel.setPaidFees(fees);
			transactionId = (Integer) session.save(studentFeesModel);
		}
		return transactionId;

	}

	/**
	 * this method updates the marks into the student_marks table
	 * 
	 * @param student TreeMap<String,Marks>
	 * @TreeMap<String,Marks>
	 **/

	public int updateMarks(TreeMap<String, StudentMarks> marksList, String studentId) {
		Connection con = DataBaseConnector.getConnection();
		int updatedSems = 0;
		try {
			PreparedStatement ps = con.prepareStatement("insert into student_marks values(?,?,?,?,?,?,?,?,?)");
			Set<String> keys = marksList.keySet();

			for (String key : keys) {
				StudentMarks marks = marksList.get(key);
				ps.setString(1, studentId);
				ps.setString(2, key);
				ps.setInt(3, marks.getSubject1());
				ps.setInt(4, marks.getSubject2());
				ps.setInt(5, marks.getSubject3());
				ps.setInt(6, marks.getSubject4());
				ps.setInt(7, marks.getSubject5());
				ps.setInt(8, marks.getSubject6());
				ps.setInt(9, marks.getSemBacklogs());

				if (ps.executeUpdate() > 0) {
					updatedSems++;
				}
			}

		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return updatedSems;
	}

	/**
	 * this method reads the marks from the student_marks table
	 * 
	 * @param studentId
	 * @TreeMap<String,StudentMarks>
	 */

	public TreeMap<String, StudentMarks> readMarks(String studentId) {
		Connection con = DataBaseConnector.getConnection();
		TreeMap<String, StudentMarks> marksList = new TreeMap<String, StudentMarks>();
		try {

			PreparedStatement ps = con.prepareStatement("select * from student_marks  where student_id = ?");
			ps.setString(1, studentId);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				StudentMarks marks = new StudentMarks();
				marks.setSubject1(rs.getInt(3));
				marks.setSubject2(rs.getInt(4));
				marks.setSubject3(rs.getInt(5));
				marks.setSubject4(rs.getInt(6));
				marks.setSubject5(rs.getInt(7));
				marks.setSubject6(rs.getInt(8));
				marks.setSemBacklogs(rs.getInt(9));
				marksList.put(rs.getString(2), marks);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return marksList;

	}

	/**
	 * this method stores the requests into the requests_table
	 * 
	 * @param studentId ,studentName,request
	 * @return true
	 */

	/**
	 * this method updates the marks into the student_marks table
	 * 
	 * @param student TreeMap<String,Marks>
	 * @TreeMap<String,Marks>
	 **/

	public boolean studentRequest(RequestModel request) {
		Connection con = DataBaseConnector.getConnection();
		boolean status = false;
		try {
			PreparedStatement ps = con.prepareStatement("insert into requests_table values(?,?,?,?,?,?)");
			// setting the system date
			long millis = System.currentTimeMillis();
			request.setDate(new java.sql.Date(millis));

			ps.setString(1, request.getStudentId());
			ps.setString(2, request.getStudentName());
			ps.setDate(3, request.getDate());
			ps.setString(4, request.getTitle());
			ps.setString(5, request.getImagSrc());
			ps.setString(6, request.getMessage());
			int result = ps.executeUpdate();
			if (result > 0) {
				status = true;
			}

		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return status;
	}

}
