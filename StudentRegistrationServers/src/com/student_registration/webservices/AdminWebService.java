package com.student_registration.webservices;

import java.awt.PageAttributes.MediaType;

import javax.websocket.server.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.server.wadl.internal.generators.resourcedoc.model.ResponseDocType;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.student_registration.models.AdminModel;
import com.student_registration.services.AdminService;
import com.student_registration.utilities.JsonDeserializer;
import com.student_registration.utilities.EnumClasses;
import com.sun.org.apache.xpath.internal.operations.Bool;

@Path("/admin")
public class AdminWebService {
	/**
	 * this method takes the request and checks the student details in database
	 * @param userName
	 * @param password
	 * @return admin json object or null 
	 */
	@GET
	@Path("/login")
	@Produces("Application/json")

	public String login(@QueryParam("userName") String userName,@QueryParam("password") String password) {
		System.out.println("hai");
		AdminService adminService = new AdminService();
		AdminModel admin = adminService.adminLogin(userName,password);
		Gson gson = new Gson();
		if(admin != null) {
			System.out.println(admin.getAdminUserName());
			
			return gson.toJson(admin);
		}
		else {
			return gson.toJson(null);
		}
	}
	/**
	 * this method compare the student name and fav place return admin unique key in database 
	 * @param userName
	 * @param securityQue
	 * @return String unique key 
	 */
	@GET
	@Path("/forgotPassword")
	
	public String forgotPassword(@QueryParam("userName") String userName,@QueryParam("securityQue") String securityQue) {
		AdminService adminService = new AdminService();
		String adminKey = adminService.adminForgotPassWord(userName, securityQue);
		return adminKey;
	
	}
	
	
	
	
	
	
	/**
	 * this method calls the sigUp method of AdminService class ...register the admin into db
	 * 
	 * @param admin is AdminModel type
	 * @return true if admin registers successfully else false
	 */
	@POST
	@Path("/signUp")
	@Produces("Application/json")


	public String signUpAdmin(String adminJson)  {
		AdminModel admin = JsonDeserializer.getObject(adminJson, AdminModel.class);
		AdminService adminService = new AdminService();
		System.out.println(admin.getAdminPassword());
		try {
		   adminService.signUpAdmin(admin);	
		   return new Gson().toJson(admin);
		   
		}catch (Exception e) {
			return new Gson().toJson(null);
		}
	}
	


	/**
	 * this method resets the admin password
	 * 
	 * @param object index or key of map ,new password details
	 * 
	 */
	
	@GET
	@Path("/resetPassword")
	
	@Produces("Application/json")
	public  String resetAdminPassword(@QueryParam("newPassword")String newPassWord,@QueryParam("key") String searchKey) {
		AdminService adminService = new AdminService();
		Boolean result = adminService.resetAdminPassword(newPassWord, Integer.parseInt(searchKey));		
	    return new Gson().toJson(result);
	
	}
	
	
	
}
