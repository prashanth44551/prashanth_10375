package com.student_registration.webservices;

import java.util.ArrayList;
import java.util.TreeMap;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.google.gson.Gson;
import com.student_registration.dao.StudentDbconnection;
import com.student_registration.enums.EnumClasses;
import com.student_registration.models.MarksInputModel;
import com.student_registration.models.SearchInputModel;
import com.student_registration.models.StudentMarks;
import com.student_registration.models.StudentModel;
import com.student_registration.models.UtilityClass;
import com.student_registration.services.StudentService;
import com.student_registration.utilities.JsonDeserializer;
import com.student_registration.utilities.UtilityDao;

@Path("/student")
public class StudentWebServices {
	StudentService studentService = new StudentService();

	Gson gson = new Gson();

	StudentDbconnection studentDbconnection = new StudentDbconnection();
	
	/**
	 * This method registers the student with student personal details
	 * 
	 * @param studentId
	 * @param fees
	 * @return
	 */
	@POST
	@Path("/registerStudent")
	@Produces("Application/json")
	public String registerStudent(String studentJson) {
		StudentModel student = JsonDeserializer.getObject(studentJson, StudentModel.class);

		Boolean result = studentService.registerStudent(student);
		return gson.toJson(result);
	}

	/**
	 * this method authenticates the student details before login
	 * 
	 * @param userName
	 * @param password
	 * @return
	 */
	@GET
	@Path("/login")
	@Produces("Application/json")
	public String login(@QueryParam("userName") String userName, @QueryParam("password") String password) {
		System.out.println("hai");
		StudentService studentService = new StudentService();
		StudentModel student = studentService.studentLogin(userName, password);
		if (student != null) {
			Gson gson = new Gson();

			String adminJson = gson.toJson(student);

			return adminJson;
		} else {
			return null;
		}
	}

	/**
	 * This method used to recover the forget password of the student
	 * 
	 * @param studentId
	 * @param newPassword
	 * @param favPlace
	 * @return
	 */
	@GET
	@Path("/forgotPassword")
	@Produces("Application/json")
	public String forgotPassword(@QueryParam("studentId") String studentId,
			@QueryParam("securityQuestion") String securityQuestion) {
		StudentService studentService = new StudentService();
		boolean result;
		String student = studentService.studentForgotPassWord(studentId, securityQuestion);
		if (student != null) {

			String adminJson = gson.toJson(true);
			return adminJson;
		} else {

			String adminJson = gson.toJson(false);
			return adminJson;
		}
	}

	/**
	 * This method used to reset the student Password
	 * 
	 * @param studentId
	 * @param newPassword
	 * @param favPlace
	 * @return
	 */
	@GET
	@Path("/resetPassword")
	@Produces("Application/json")
	public String resetPassword(@QueryParam("studentId") String studentId,
			@QueryParam("newPassword") String newPassword, @QueryParam("favPlace") String favPlace) {

		boolean student = studentService.resetPassword(studentId, newPassword, favPlace);
		if (student) {

			String adminJson = gson.toJson(student);
			return adminJson;
		} else {

			String adminJson = gson.toJson(student);
			return adminJson;
		}
	}

	/**
	 * This method deletes the Existing student details with the help of student Id
	 * 
	 * @param studentId
	 * @return
	 */
	@GET
	@Path("/deleteStudent")
	@Produces("Application/json")
	public String deleteStudent(@QueryParam("studentId") String studentId) {
		Boolean status = studentService.deleteStudent(studentId);
		return status.toString();

	}

	/**
	 * This method updates the student Details
	 * 
	 * @param studentId
	 * @param fieldName
	 * @param value
	 * @return
	 */
	@GET
	@Path("/updateStudent")
	@Produces("Application/json")
	public String updateStudent(@QueryParam("studentId") String studentId, @QueryParam("fieldName") String fieldName,
			@QueryParam("value") String value) {
		Boolean status = studentService.updateStudent(studentId, fieldName, value) ;
		return status.toString();

	}

	@POST
	@Path("/search")
	@Produces("Application/json")
	public String search(String searchInputJson) {
		//converting the input json  into SearchInputModel type 
		SearchInputModel searchInput = JsonDeserializer.getObject(searchInputJson, SearchInputModel.class);
		StudentService studentService = new StudentService();
		System.out.println(searchInput.getSearchContext()+" "+searchInput.getInput());
		//getting the data from the database 
		ArrayList<StudentModel> students = studentService.search(searchInput.getSearchContext(), searchInput.getInput(),
				searchInput.getSearchList());
		Gson gson = new Gson();
		return gson.toJson(students);

	}
	
	
	/**
	 * This method update the student Marks
	 * 
	 * @param studentId
	 * @param fees
	 * @return
	 */
	@POST
	@Path("/updateMarks")
	@Produces("Application/json")
	 public String updateMarks(String marksInputJson) {
	    MarksInputModel marksInput  = JsonDeserializer.getObject(marksInputJson, MarksInputModel.class);
	    	int result = studentService.updateMarks(marksInput.getStudentMarksList() , marksInput.getStudentId());
	    	if (result>0){
	    		return gson.toJson(true);
	    		
	    	} else {
	    		return gson.toJson(false);
	    	}
	    }
	/**
	 * This method calls the student service readmarks method and fetch the marks of a student;
	 * @param studentId
	 * @return
	 */
	@GET
	@Path("/readMarks")
	@Produces("Application/json")
	public String readMarks(@QueryParam("studentId") String studentId){
		TreeMap<String, StudentMarks> studentMarksList = studentService.readMarks(studentId);
		if(studentMarksList != null) {
			return gson.toJson(studentMarksList);
		} else {
			return gson.toJson(false);
		}
	}
	
	

	/**
	 * This method updates the student fees based on studentId
	 * 
	 * @param studentId
	 * @param fees
	 * @return
	 */
	@GET
	@Path("/updateFees")
	@Produces("Application/json")
	public String updateFees(@QueryParam("studentId") String studentId, @QueryParam("fees") double fees) {
		  Boolean status = studentService.updateFees(studentId, fees);
	      return status.toString();
	}
	/**
	 * this method calls the database and gives the next student id to the last studentId present db
	 * @return String studentId
	 * @throws Exception
	 */
	@GET
	@Path("/getNewStudentId")
	public String getNewStudentId(){
		return studentService.getNewStudentId();
	}
	@GET 
	@Path("/feesDetails")
	public String getFeesDetails(@QueryParam("studentId") String studentId){
		TreeMap<String,Double> feesList = studentService.getFeesDetails(studentId);
		return gson.toJson(feesList);
	}
}
