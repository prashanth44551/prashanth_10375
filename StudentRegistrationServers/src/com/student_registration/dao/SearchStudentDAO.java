package com.student_registration.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import com.student_registration.enums.EnumClasses;
import com.student_registration.models.StudentModel;
import com.student_registration.utilities.DataBaseConnector;
import com.student_registration.utilities.UtilityDao;

public class SearchStudentDAO {
	StudentModel student;
	public final static int SYSTEM_YEAR = Calendar.getInstance().get(Calendar.YEAR);


	/**
	 * thsis method returns the list of students if matches the required search
	 * pattern
	 * 
	 * @param context
	 * @param input
	 * @param searchedList
	 * @return
	 */
		
	public ArrayList<StudentModel> search(EnumClasses.SearchKeys context, String input,ArrayList<StudentModel> searchedList) {
          // this is for already serached students
			if(searchedList != null && !searchedList.isEmpty()) {
				return studentSearchFilters(context, input, searchedList);
			}


		ArrayList<StudentModel> foundStudents = new ArrayList<StudentModel>();
		Connection con = DataBaseConnector.getConnection();
		PreparedStatement ps;
		ResultSet rs;
		UtilityDao util = new UtilityDao();
		try {
			switch (context) {
			  
			case Id :
				 foundStudents.add(new UtilityDao().fetchStudentDetails(input));
				 
				
			case ALL:
				ps = con.prepareStatement("select studentId from student_table order by studentId");
				rs = ps.executeQuery();
				while (rs.next()) {
					foundStudents.add(util.fetchStudentDetails(rs.getString(1)));
				}
				break;
			case COURSE:
				ps = con.prepareStatement("select studentId from student_table  where course =? ");
				ps.setString(1, input);
				rs = ps.executeQuery();
				while (rs.next()) {
					foundStudents.add(util.fetchStudentDetails(rs.getString(1)));
				}

				break;

			case NAME:
				ps = con.prepareStatement("select studentId from student_table  where firstname like ? or lastname like  ? ");
				input = "%"+input+"%";
				ps.setString(1, input);
				ps.setString(2, input);
				rs = ps.executeQuery();
				while (rs.next()) {
					foundStudents.add(util.fetchStudentDetails(rs.getString(1)));
				}
				break;
			case DUE:
				
				ps = con.prepareStatement("select studentId from student_table");
				rs = ps.executeQuery();
				while (rs.next()) {
					StudentModel student = util.fetchStudentDetails(rs.getString(1));
					if (student.getDue() > 0) {
						foundStudents.add(student);
					}
				}
				break;
			case CAST:
				ps = con.prepareStatement("select studentId from student_table  where caste =? ");
				ps.setString(1, input);
				rs = ps.executeQuery();
				while (rs.next()) {
					foundStudents.add(util.fetchStudentDetails(rs.getString(1)));
				}
				break;
			case YEAR:
				int reqJoiningYear = SYSTEM_YEAR - Integer.parseInt(input);
				ps = con.prepareStatement(
						"select studentId from student_table where joiningYear = ? or joiningYear = ? ");
				ps.setInt(1, reqJoiningYear);
				ps.setInt(2, reqJoiningYear + 1);
				rs = ps.executeQuery();
				while (rs.next()) {
					student = util.fetchStudentDetails(rs.getString(1));
					if (student.getPusrsuingYear() == Integer.parseInt(input)) {
						foundStudents.add(student);
					}
				}
				break;
			case BACKLOGS:
				ps = con.prepareStatement("select student_id  from student_marks  where sem_Wise_Backlogs > 0 group by(student_id) ");
				rs = ps.executeQuery();
				while (rs.next()) {
					foundStudents.add(util.fetchStudentDetails(rs.getString(1)));
				}
				break;
			case GENDER:
				ps = con.prepareStatement("select studentId from student_table  where gender =? ");
				ps.setString(1, input);
				rs = ps.executeQuery();
				while (rs.next()) {
					foundStudents.add(util.fetchStudentDetails(rs.getString(1)));
				}
			default:

				break;
			}

//			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				con.close();
			} catch (SQLException e) {}
		}

		return foundStudents;
	}
	
	
	
/**
 * This method filters the student data from already searchsed data 
 * 
 *@param ArrayList<StduentModel>,EnumClasses.SearchKeys context, String input,
 *@return ArrayList<StudentModel>
 */
	
	public ArrayList<StudentModel> studentSearchFilters(EnumClasses.SearchKeys context, String input,ArrayList<StudentModel> studentsList) {
		ArrayList<StudentModel> filteredStudents = new ArrayList<StudentModel>();
		for (StudentModel student : studentsList) {
			switch (context) {
			case Id:
				if(student.getStudentId().equals(input)) {
					 filteredStudents.add(student);
				}
			
			case ALL:
			    filteredStudents.add(student);
			    break;
			case COURSE:
				if (student.getCourse().toLowerCase().contains(input)) {
					filteredStudents.add(student);
				}
				break;
	
			case NAME:
				if (student.getFirstName().toLowerCase().contains(input)
						|| student.getLastname().toLowerCase().contains(input)) {
					filteredStudents.add(student);
				}
				break;
			case DUE:
				if (student.getDue() > 0) {
					filteredStudents.add(student);
				}
				break;
			case CAST:
				if (student.getCaste().toLowerCase().contains(input)) {
					filteredStudents.add(student);
				}
				break;
			case YEAR:
				int studentCurrentYear = student.getPusrsuingYear();
				if (studentCurrentYear == Integer.parseInt(input)) {
					filteredStudents.add(student);
				} else if (studentCurrentYear > 5 && Integer.parseInt(input) == 5) {
					filteredStudents.add(student);
				}
				break;
			case BACKLOGS:
				if (student.getBacklogs() > 0) {
					filteredStudents.add(student);
				}
				break;
			case GENDER:
				if(student.getGender().equalsIgnoreCase(input)) {
					filteredStudents.add(student);
				}
	
			default:
				break;
			}
	
		}
		return filteredStudents;
    }
	
	
}
