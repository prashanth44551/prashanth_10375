package com.student_registration.test;

import static org.junit.Assert.assertEquals;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Set;
import java.util.TreeMap;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.student_registration.dao.SearchStudentDAO;
import com.student_registration.enums.EnumClasses;
import com.student_registration.models.StudentMarks;
import com.student_registration.models.StudentModel;
import com.student_registration.services.StudentService;
import com.student_registration.utilities.UtilityDao;

/**
 * This class tests student operations
 * 
 * @author IMVIZAG
 *
 */
public class StudentTest {

	StudentService studentservice = null;

	@Before
	@Test
	public void setUp() {
		studentservice = new StudentService();
		System.out.println("before");
	}

	@After
	@Test
	public void tearDown() {
		studentservice = null;
		System.out.println("after");
	}

	@Test
	public void studentLoginTest() {

		assertNotNull(studentservice.studentLogin("CN0010002", "123456"));

	}

	@Test
	public void studentNegativeLoginTest() {

		assertNull(studentservice.studentLogin("CN0010002", "Inn0@123"));

	}

	@Test
	public void studentForgotPasswordTest() {

		assertNull(studentservice.studentForgotPassWord("CN0010005", "rajam"));

	}

	@Test
	public void resetPasswordPositiveTest() {
		assertNotNull(studentservice.resetPassword("123456", "CN0010005", "hyd"));
	}

	@Test
	public void deleteStudentPositiveTest() {
		assertTrue(studentservice.deleteStudent("CN0010004"));
	}

	@Test
	public void readMarksPositiveTest() {
		assertNotNull(studentservice.readMarks("CN0010005"));
	}

	@Test
	public void updateFeesPositiveTest() {
		assertTrue(studentservice.updateFees("CN0010003", 1234));
	}

	@Test
	public void updateMarksPositiveTest() {
		TreeMap<String, StudentMarks> marksList = new TreeMap<String, StudentMarks>();
		StudentMarks marks = new StudentMarks();
		marks.setSubject1(10);
		marks.setSubject2(47);
		marks.setSubject3(80);
		marks.setSubject4(90);
		marks.setSubject5(30);
		marks.setSubject6(80);
		marks.setSemBacklogs(2);

		marksList.put("sem2", marks);
        assertEquals(studentservice.updateMarks(marksList, "CN0010005"), 1);
	}

	@Test
	public void updateStudentPositiveTest() {
		assertTrue(studentservice.updateStudent("CN0010003", "firstName", "raj"));
	}

	@Test
	public void registerStudentPositiveTest() {
		StudentModel student = new StudentModel();
		student.setStudentId("CN0010005");
		student.setFirstName("prashanth");
		student.setLastname("kumar");
		student.setDateOfBirth("12-2-1999");
		student.setMobileNo("9234567899");
		student.setEmail("pk@gmail.com");
		student.setGender("male");
		student.setQualification("Inter");
		student.setCaste("oc");
		student.setCourse("btech");
		student.setJoiningYear(2017);
		student.setAddress("gajuwaka");
		assertTrue(studentservice.registerStudent(student));
	}
   @Test
	public void searchByIdPositiveTest() {
	   assertTrue(studentservice.search(EnumClasses.SearchKeys.Id, "CN0010001", null).size() > 0);
		
   }
   @Test
 	public void searchPositiveTest() {
		assertTrue(studentservice.search(EnumClasses.SearchKeys.ALL, "ALL", null).size() > 0);
    }
   
}
