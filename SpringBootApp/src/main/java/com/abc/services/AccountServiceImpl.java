package com.abc.services;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abc.dao.AccountRepository;
import com.abc.entities.Account;
@Service
@Transactional
public class AccountServiceImpl  implements AccountService{
	@Autowired
     private AccountRepository accrepo;
	@Override
	public Account save(Account account) {
		// TODO Auto-generated method stub
		return accrepo.save(account);
	}

	@Override
	public List<Account> findAll() {
		// TODO Auto-generated method stub
		 List<Account> accountList = new ArrayList<Account>();
//		 repository.findAll().forEach(e -> accountList.add(e));
		 Iterable<Account> iAccount = accrepo.findAll();
		 Iterator<Account> iterator = iAccount.iterator();
		
		 while(iterator.hasNext()) {
			 accountList.add(iterator.next());
		 }
		 return accountList;
	}

	@Override
	public Account findAccountById(int accno) {
		
		return  accrepo.findById(accno).get();
	}

	@Override
	public void deleteAccount(int accno) {
		accrepo.deleteById(accno);
		
	}

	@Override
	public Account updateAccount(Account account) {
		// TODO Auto-generated method stub
		return accrepo.save(account);
	}

}
