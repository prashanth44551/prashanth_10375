package com.abc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.abc.bean.Employee;
import com.abc.service.EmployeeService;



@Controller
public class EmployeeController {
	
	 @Autowired
	 private EmployeeService employeeService;
	 
	

	public EmployeeService getEmployeeService() {
		return employeeService;
	}



	public void setEmployeeService(EmployeeService employeeService) {
		this.employeeService = employeeService;
	}



	public Employee createEmp() {
		
		Employee employee=employeeService.createEmp();
		return  employee;
		
	}

}
