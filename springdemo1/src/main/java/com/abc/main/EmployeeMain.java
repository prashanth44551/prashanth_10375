package com.abc.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.abc.bean.Employee;
import com.abc.controller.EmployeeController;

public class EmployeeMain {
	
public static void main(String[] args) {
		
		// open/read the application context file	
	ApplicationContext context = new ClassPathXmlApplicationContext("classpath:/com/abc/resources/context.xml");
	//instantiate our spring EmployeeController object from the application context
	EmployeeController controller=(EmployeeController) context.getBean(EmployeeController.class); 
	Employee emp = (Employee) context.getBean("employee"); 
//	Employee emp= controller.createEmp();
	
	  System.out.println("Employee saved:"+emp.getEmp_Id());
	  System.out.println("Name: "+ emp.getName());
		System.out.println("City: "+ emp.getAddress().getCity());
		System.out.println("State: "+ emp.getAddress().getState());
	  
	}

}
