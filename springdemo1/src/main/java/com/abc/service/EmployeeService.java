package com.abc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abc.bean.Address;
import com.abc.bean.Employee;
import com.abc.dao.EmployeeDAO;


@Service
public class EmployeeService {
	@Autowired
	EmployeeDAO employeeDAO;
	
	public Employee createEmp() {
		Employee employee=employeeDAO.createEmp();
		return employee;
		
	}

	public EmployeeDAO getEmployeeDAO() {
		return employeeDAO;
	}

	public void setEmployeeDAO(EmployeeDAO employeeDAO) {
		this.employeeDAO = employeeDAO;
	}
	
	




}