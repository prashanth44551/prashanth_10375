package com.abc.service;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abc.bean.Account;
import com.abc.dao.AccountDAO;

@Service
public class AccountServiceImpl implements AccountByService {
	@Autowired
    private AccountDAO accountdao;

	
	@Override
	@Transactional
	public Account searchById(int accno) {
		
		return accountdao.getAccountByID(accno);
	}
	@Transactional
	@Override
	public Account deleteById(int accno) {
		
		return accountdao.deleteByAccId(accno);
	}
	@Transactional
	@Override
	public int createNewAccount(Account account) {
		
		return accountdao.createAccount(account);
	}
	
	
	@Transactional
	@Override
	public void updateDetailsById(Account currentAccount) {
		
	accountdao.updateDetailsById(currentAccount);
	}



}
