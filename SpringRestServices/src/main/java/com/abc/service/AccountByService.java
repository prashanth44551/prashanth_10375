package com.abc.service;

import com.abc.bean.Account;

public interface AccountByService {
     Account searchById(int accno);
     Account deleteById(int accno);
    int createNewAccount(Account account);
	void updateDetailsById(Account currentAccount);

	

}
