package com.abc.dao;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


import com.abc.bean.Account;

@Repository
public class AccountDAOImpl implements AccountDAO {
	
   @Autowired
   private SessionFactory sessionfactory;
   @Override
   public Account getAccountByID(int id) {
	
		Session session=sessionfactory.getCurrentSession();
		
		Account account=session.get(Account.class, id);
		session.save(account);
		return account;
		
	}
   
	@Override
	public Account deleteByAccId(int accno) {
	
		Session session=sessionfactory.getCurrentSession();
		Account account=session.get(Account.class, accno);
		
		session.delete(account);
		return account;
	
	}
	
	@Override
	public int createAccount(Account account) {
		
		Session session=sessionfactory.getCurrentSession();
		int acc=account.getAccno();
		session.save(account);
		return acc;
	}
	

	
	@Override
	public void updateDetailsById(Account currentAccount) {
      Session session=sessionfactory.getCurrentSession();
	
		session.update(currentAccount);
		
		

	}
	
	

}
