package com.abc.dao;

import com.abc.bean.Account;

public interface AccountDAO {
       Account getAccountByID(int id);

	  void updateDetailsById(Account currentAccount);

	  Account deleteByAccId(int accno);

	  int createAccount(Account account);
	
}
