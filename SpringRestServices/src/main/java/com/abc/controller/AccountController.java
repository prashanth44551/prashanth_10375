package com.abc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.abc.bean.Account;
import com.abc.service.AccountByService;

@RestController
public class AccountController {
	
	
	@Autowired
	private AccountByService accountservice;
	
	@GetMapping("create")
	public String getRegisterForm() {
		return "createNewAccount";
	}
	@GetMapping("update")
	public String updateAccountDetails() {
		return "updateAccountDetails";
	}
	
	@GetMapping("/accounts/{id}")
   public ResponseEntity<Account>  searchById(@PathVariable("id") int accno)
   {
	   Account account=accountservice.searchById(accno);
	   if (account == null) {
	  
       return new ResponseEntity<Account>(HttpStatus.NOT_FOUND);
   }
   return new ResponseEntity<Account>(account, HttpStatus.OK);
}
   
   
   
   
   @GetMapping("/{id}")
 public ResponseEntity<Account> deleteById(@PathVariable("id") int accno ) 
 {
	 Account acc=accountservice.searchById(accno);
	
     if (acc == null) {
         
         return new ResponseEntity<Account>(HttpStatus.NOT_FOUND);
     }
     accountservice.deleteById(accno);
     return new ResponseEntity<Account>(HttpStatus.NO_CONTENT);
  
}
   @CrossOrigin
  @RequestMapping(value="/createNewAccount",method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE)

   public ResponseEntity<Account> createNewAccount(@RequestBody Account account) 
   {
	   int id=accountservice.createNewAccount(account);
	   return new ResponseEntity<Account>(account, HttpStatus.CREATED);
 			}
   
   
   @PutMapping("/update/{id}")
   public ResponseEntity<Account> update(@PathVariable("id") int accno, @RequestBody Account currentAccount) {
	   Account account = accountservice.searchById(accno);
         
        if (account==null) {
           return new ResponseEntity<Account>(HttpStatus.NOT_FOUND);
        }
 
        account.setAccno(currentAccount.getAccno());
        account.setName(currentAccount.getName());
        account.setBalance(currentAccount.getBalance());
        accountservice.updateDetailsById(account);
        return new ResponseEntity<Account>(account, HttpStatus.OK);
	}
   }
   

