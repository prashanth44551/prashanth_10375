package com.abc.bean;

import org.springframework.stereotype.Component;

@Component
public class Employee {
	
	private int emp_Id;

	
	private String name;
 
 	private Address address;
	
	
	public int getEmp_Id() {
		return emp_Id;
	}
	public void setEmp_Id(int emp_Id) {
		this.emp_Id = emp_Id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
}
