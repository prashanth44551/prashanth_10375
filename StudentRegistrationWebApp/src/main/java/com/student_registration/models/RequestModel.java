package com.student_registration.models;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "requests_table")
public class RequestModel {
    
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	
	int requestId;
	String studentId;
    String studentName;
	Date date;
	String title;
	@Column(name = "image_src")
	String imageSrc;
	String message;
     
	/**
	 * @return the id
	 */
	public int getRequestId() {
		return requestId;
	}

	/**
	 * @param id the id to set
	 */
	public void setRequestId(int id) {
		this.requestId = id;
	}

	/**
	 * @return the imageSrc
	 */
	public String getImageSrc() {
		return imageSrc;
	}

	/**
	 * @param imageSrc the imageSrc to set
	 */
	public void setImageSrc(String imageSrc) {
		this.imageSrc = imageSrc;
	}

	/**
	 * 
	 * 
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the imagSrc
	 */
	public String getImagSrc() {
		return imageSrc;
	}

	/**
	 * @param imagSrc the imagSrc to set
	 */
	public void setImagSrc(String imagSrc) {
		this.imageSrc = imagSrc;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

    
    
    
    
/**
 * @return the studentId
 */
public String getStudentId() {
	return studentId;
}
/**
 * @param studentId the studentId to set
 */
public void setStudentId(String studentId) {
	this.studentId = studentId;
}
/**
 * @return the studentName
 */
public String getStudentName() {
	return studentName;
}
/**
 * @param studentName the studentName to set
 */
public void setStudentName(String studentName) {
	this.studentName = studentName;
}
}
