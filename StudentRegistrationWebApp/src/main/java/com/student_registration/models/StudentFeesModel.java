package com.student_registration.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "student_fees_details")
public class StudentFeesModel {
	@Id
	@Column(name = "transaction_id")
	private int transactionId;
	@Column(name = "paid_fees")
	private double paidFees;
	@Column(name = "paid_date")
	private String paidDate;
	@Column(name = "studentId")
	private String studentId;

	/**
	 * @return the transactionId
	 */
	public int getTransactionId() {
		return transactionId;
	}

	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}

	/**
	 * @return the paidFees
	 */
	public double getPaidFees() {
		return paidFees;
	}

	/**
	 * @param paidFees the paidFees to set
	 */
	public void setPaidFees(double paidFees) {
		this.paidFees = paidFees;
	}

	/**
	 * @return the paidDate
	 */
	public String getPaidDate() {
		return paidDate;
	}

	/**
	 * @param paidDate the paidDate to set
	 */
	public void setPaidDate(String paidDate) {
		this.paidDate = paidDate;
	}

	/**
	 * @return the studentId
	 */
	public String getStudentId() {
		return studentId;
	}

	/**
	 * @param studentId the studentId to set
	 */
	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

}
