package com.student_registration.models;

import java.util.ArrayList;

public class StudentHomeModel {
  ArrayList<PostModel> newsFeed;
  StudentModel student;
/**
 * @return the newsFeed
 */
public ArrayList<PostModel> getNewsFeed() {
	return newsFeed;
}
/**
 * @param newsFeed the newsFeed to set
 */
public void setNewsFeed(ArrayList<PostModel> newsFeed) {
	this.newsFeed = newsFeed;
}
/**
 * @return the student
 */
public StudentModel getStudent() {
	return student;
}
/**
 * @param student the student to set
 */
public void setStudent(StudentModel student) {
	this.student = student;
}
}
