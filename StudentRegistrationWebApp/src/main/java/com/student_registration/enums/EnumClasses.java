package com.student_registration.enums;

public class EnumClasses {
	public static enum SearchKeys{
		Id,
		ALL,
		NAME,
		COURSE,
		DUE,
		CAST,
		YEAR,
		BACKLOGS,
		GENDER
	}
}
