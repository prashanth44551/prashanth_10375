package com.student_registration.dao;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;

import com.student_registration.models.AdminHomeModel;
import com.student_registration.models.AdminModel;
import com.student_registration.models.PostModel;
import com.student_registration.models.RequestModel;
import com.student_registration.utilities.DataBaseConnector;
import com.student_registration.utilities.UtilityDao;

/**
 * this class is created for doing all the CURD operations into the files
 * 
 * @author Group B
 *
 */
public class AdminDbConnection {
	public static final String UNIQKEY = "GROUP-B";
	UtilityDao utilityDao = new UtilityDao();
	SessionFactory sf = null;
	Session session = null;
	
	public AdminDbConnection() {
		sf = DataBaseConnector.getSessionFactory();
		session = sf.openSession();
	}
	
	public void signUpAdmin(AdminModel admin) throws Exception {
		
		if (admin.getUniqueId().equals(UNIQKEY)) {
			if(session == null) {
				throw new Exception("sorry some thing went wrong");
			}
			Transaction transaction = session.beginTransaction();
			Integer adminId = (int)session.save(admin);
			
			transaction.commit();
		
			if (adminId == null) {
				throw new Exception("sorry some thing went wrong");
			}
	    }else {
			
			throw new Exception("Entered uniqueId is not matched");
		}
	}

	/**
	 * this method checks the admin details,if found return the boolen value
	 * 
	 * @param adminInfo
	 * @return true if the admin is Valid admin else return false
	 */

	public AdminHomeModel adminLogin(String username, String password) {
		AdminModel admin = null;
		AdminHomeModel  adminHomeModel = null;
		
			Transaction transaction = session.beginTransaction();
			
			Criteria criteria = session.createCriteria(AdminModel.class);
			Criterion name = Restrictions.ilike("adminUserName",username);
			Criterion pass = Restrictions.ilike("adminPassword",password);
		    LogicalExpression exp = Restrictions.and(name,pass);
			criteria.add(exp);
		    admin = (AdminModel)criteria.uniqueResult(); 
		    transaction.commit();
			if(admin != null) {
				// CREATING THE ADMIN HOME AND ADDING THE REQUESTS AND ADMIN OBJECT
				adminHomeModel = new AdminHomeModel();
				adminHomeModel.setAdmin(admin);
				adminHomeModel.setRequests(getRequests());
			}

	
		return adminHomeModel;

	}

	/**
	 * this method searching the admin by the username and security code returns the
	 * key of the admin
	 * 
	 * @param adminForgotDetails
	 * @return admin index in treeMap of admins
	 */

	public String adminForgotPassWord(String userName, String securityQue) {
        AdminModel admin = null;
		Transaction transaction = session.beginTransaction();
		
		Criteria criteria = session.createCriteria(AdminModel.class);
		Criterion name = Restrictions.ilike("adminUserName",userName);
		Criterion seq = Restrictions.ilike("adminSecurityQue",securityQue);
	    LogicalExpression exp = Restrictions.and(name,seq);
		criteria.add(exp);
	    admin = (AdminModel)criteria.uniqueResult(); 
	    transaction.commit();
		if(admin != null) {
			return String.valueOf(admin.getEmpId());
		}

		return null;
	}

	/**
	 * this method resets the admin password
	 * 
	 * @param object index or key of map ,new password details
	 * 
	 */
	public boolean resetAdminPassword(String newPassWord, int searchKey) {
		Transaction transaction =session.beginTransaction();
		Query query = session.createQuery("update AdminModel set adminPassword=:newPassword where empId=:employeeId");
		query.setParameter("newPassword", newPassWord);
	    query.setParameter("employeeId", searchKey);
	    int res = query.executeUpdate();
	    transaction.commit();
		if (res > 0)
		  return true;

		return false;

	}
	
	
	/**
	 * this methiod posts the data into the Posts table
	 * 
	 */
	
	public boolean postFeed(PostModel adminPost) {
		Transaction transaction = session.beginTransaction();
		Integer postId = (Integer)session.save(adminPost);
		transaction.commit();
		
		if(postId != null)
			return true;
		return false;

	}
	
	/**
	 * this method reads the requests from the requests table
	 * @return arrayList of requestModel objects
	 */
	public ArrayList<RequestModel> getRequests() {
		ArrayList<RequestModel> requests = new ArrayList<RequestModel>();
		Query query = session.createQuery("from RequestModel order by date desc");
			requests = new ArrayList<RequestModel>(query.list());
		return requests;

	}
	


}
