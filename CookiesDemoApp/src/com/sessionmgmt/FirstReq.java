package com.sessionmgmt;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.Key;
import java.util.Base64;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.crypto.MacProvider;

/**
 * Servlet implementation class FirstReq
 */
@WebServlet("/FirstReq")
public class FirstReq extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Gson gson = new Gson();
	private static final Key secret = MacProvider.generateKey(SignatureAlgorithm.HS256);
    private static final byte[] secretBytes = secret.getEncoded();
    private static final String base64SecretBytes = Base64.getEncoder().encodeToString(secretBytes);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username = "admin";
		String password = request.getParameter("pwd");
		
//		if(username.equals("admin") && password.equals("admin")) {
			String token = JwtImpl.generateToken(username);
			String serverTokenId = JwtImpl.verifyToken(token);
			PrintWriter out = response.getWriter();
			
			//setting json content type
			response.setContentType("application/json");
			
			response.setCharacterEncoding("UTF-8");
			
			
			String[] data = {token,username};
			
			String studentJsonString = this.gson.toJson(data);
			
			
			String userTokenId = JwtImpl.verifyToken(token);
			
			
			if(serverTokenId.equals(userTokenId)) {
				out.println("same User");
				out.println(serverTokenId);
			}else {
				out.println("unauthorized  user");
			}
			
			// writting object into stream
			out.print(studentJsonString);
			
			out.flush();
//		}
		
	}

}
