package com.abc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.abc.bean.Account;
import com.abc.service.AccountByService;

@Controller
public class AccountController {
	
	
	@Autowired
	private AccountByService accountservice;
	
	@GetMapping("create")
	public String getRegisterForm() {
		return "createNewAccount";
	}
	@GetMapping("update")
	public String updateAccountDetails() {
		return "updateAccountDetails";
	}
	
   @RequestMapping("/accounts/{id}")
   public String searchById(@PathVariable("id") int accno,ModelMap map)
   {
	   Account acc=accountservice.searchById(accno);
	   map.addAttribute("accoun", acc);
	   return "searchById";
   }
   
   
   
   @RequestMapping("/{id}")
 public String deleteById(@PathVariable("id") int accno ,ModelMap map) 
 {
	 boolean acc=accountservice.deleteById(accno);
	 map.addAttribute("accno", acc);
	 if(acc)
	 {
	 return "deleteById";
	 }
	
		 return "notDeleted";
	 
 }
   
   
   @PostMapping("/createNewAccount")
   public String createNewAccount(@RequestParam("accno") int accno,
		   @RequestParam("name") String name, @RequestParam("balance") double balance,ModelMap map) 
   {
  	 Account acc=accountservice.createNewAccount(accno,name,balance);
  	 map.addAttribute("accno", acc);
  	if(acc!=null)
  	{
  	 return "createdById";
  	}
  	return "notCreated";
  	
   }
   
   
   @PostMapping("/updateAccountDetails")
   public String updateDetails(@PathVariable("accno") int accno,@PathVariable("balance") double balance,@PathVariable("name") String name)
   {
	   Account account=accountservice.updateDetailsById(accno,name,balance);
	   if(account!=null) {
	return "detailsUpdated";
	   }
	   return "DetailsNotUpdated";
   }
   }
   

