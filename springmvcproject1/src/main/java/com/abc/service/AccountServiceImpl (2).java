package com.abc.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abc.bean.Account;
import com.abc.dao.AccountDAO;

@Service
public class AccountServiceImpl implements AccountByService {
	@Autowired
    private AccountDAO accountdao;

	
	@Override
	@Transactional
	public Account searchById(int accno) {
		
		return accountdao.getAccountByID(accno);
	}
	@Transactional
	
	public boolean deleteById(int accno) {
		
		return accountdao.deleteByAccId(accno);
	}
	@Transactional
	public Account createNewAccount(int accno, String name, double balance) {
		
		return accountdao.createAccount(accno, name, balance);
	}
	
	@Transactional
	public Account updateDetailsById(int accno, String name, double balance) {
		
		return accountdao.updateDetailsById(accno,name,balance);
	}


}
