package com.abc.service;

import com.abc.bean.Account;

public interface AccountByService {
     Account searchById(int accno);
     boolean deleteById(int accno);
	
	Account createNewAccount(int accno, String name, double balance);
	
	Account updateDetailsById(int accno, String name, double balance);

	

}
