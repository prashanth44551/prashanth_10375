package com.abc.dao;

import com.abc.bean.Account;

public interface AccountDAO {
       Account getAccountByID(int id);

	Account createAccount(int accno, String name, double balance);

	Account updateDetailsById(int accno, String name, double balance);

	boolean deleteByAccId(int accno);
	
}
