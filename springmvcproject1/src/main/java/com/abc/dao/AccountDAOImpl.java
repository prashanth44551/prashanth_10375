package com.abc.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.abc.bean.Account;

@Repository
public class AccountDAOImpl implements AccountDAO {
	
   @Autowired
   private SessionFactory sessionfactory;
   @Autowired
   private Account account;

   public Account getAccount() {
		return account;
	}
	public void setAccount(Account account) {
		this.account = account;
	}
	  @Transactional
	  @Override
   public Account getAccountByID(int id) {
	
		Session session=sessionfactory.getCurrentSession();
		
		Account account=session.get(Account.class, id);
		return account;
		
	}
    @Transactional
	@Override
	public boolean deleteByAccId(int accno) {
	
		Session session=sessionfactory.getCurrentSession();
		Account account=session.get(Account.class, accno);
		if(account!=null)
		{
		session.delete(account);
		return true;
		}
		
		return false;
	}
	@Transactional
	@Override
	public Account createAccount(int accno, String name, double balance) {
		
		Session session=sessionfactory.getCurrentSession();
		account.setAccno(accno);
		account.setBalance(balance);
		account.setName(name);
	
		session.save(account);
		return account;
	}
	

	@Transactional
	@Override
	public Account updateDetailsById(int accno,String name, double balance) {
//      Session session=sessionfactory.getCurrentSession();
//		for(Account acc:acc1)
//		{
//			if(acc.getAccno()==accno)
//			{
//				a++;
//				account.setBalance(balance);
//				account.setName(name);
//				session.save(account);
//				break;
//			}
//		}		
//		
//		if(a!=0)
//		session.save(account);
//		return account;
//
		return null;
	}
	
	

}
