package com.abc.springdemo;



import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.cba.Employee;

public class Main {
  public static void main(String[] args)
  {

	  ApplicationContext app=new ClassPathXmlApplicationContext("classpath:com/abc/springdemo/config.xml");
	  Employee emp=(Employee) app.getBean("emp");
	  System.out.println(emp.getEmpId());
  }
}
