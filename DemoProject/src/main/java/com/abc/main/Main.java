package com.abc.main;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import com.abc.entities.Product;
import com.abc.util.HibernateUtil;

public class Main {
@SuppressWarnings("unchecked")
public static void main(String[] args) {
	Product product1=new Product();
	product1.setProductcategory("soap");
	product1.setPrice(59);
	product1.setProductid("19");
	product1.setProductname("lux");

	SessionFactory sessionfactory =HibernateUtil.getSessionFactory();
	Session session =sessionfactory.openSession();
	CriteriaBuilder cb = session.getCriteriaBuilder();
	CriteriaQuery<Product> cr = cb.createQuery(Product.class);
	Root<Product> root = cr.from(Product.class);
	cr.select(root).where(cb.lt(root.get("price"), 70));
	cr.from(Product.class);
	
	Query query = session.createQuery(cr);
	List<Product> results = query.getResultList();
	
     for(Product p:results)
		{
			System.out.println(p.getPrice()+""+p.getProductcategory()+""+p.getProductid());
		}
	
	
	 session.close();
}
}
