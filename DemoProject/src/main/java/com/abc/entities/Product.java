package com.abc.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="product")
public class Product {
	

	@Id
    @Column(name="product_id")
private String productid;


private int price;
	
	@Column(name="product_name")
private String productname;

	@Column(name="product_category")
private String productcategory;
	
	
	
public String getProductid() {
	return productid;
}
public void setProductid(String productid) {
	this.productid = productid;
}
public int getPrice() {
	return price;
}
public void setPrice(int price) {
	this.price = price;
}
public String getProductname() {
	return productname;
}
public void setProductname(String productname) {
	this.productname = productname;
}
public String getProductcategory() {
	return productcategory;
}
public void setProductcategory(String productcategory) {
	this.productcategory = productcategory;
}

}
